import javax.accessibility.AccessibleContext;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;

public class Window {
    private JPanel panel1;
    private JPanel centerpanel;
    private JButton Zapisz;
    private JTextField imieField1;
    private JTextField nazwiskoField;
    private JSpinner butspinner;
    private JTextField wiekField;
    private JTextField emailField;
    private JComboBox stancomboBoc;
    private JButton Odczyt;
    private JRadioButton Kobieta;
    private JRadioButton Mężczyzna;
    private JPanel Plec;
    private JLabel Etykieta;


    public Window() {
        Kobieta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // usun z combobox'a czesc opcji
                // dodaj pozostale
                stancomboBoc.removeAllItems();
                stancomboBoc.addItem("mężatka");
                stancomboBoc.addItem("singielka");
            }
        });
        Mężczyzna.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // usun z combobox'a czesc opcji
                // dodaj pozostale
                stancomboBoc.removeAllItems();
                stancomboBoc.addItem("żonaty");
                stancomboBoc.addItem("singiel");
            }
        });

        Zapisz.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String imie = imieField1.getText();
                String nazwisko = nazwiskoField.getText();
                int numerbuta = (int) (butspinner.getValue());
                String plec = Kobieta.isSelected() ? "Kobieta" : "Mężczuzna";
                String wiek = wiekField.getText();
                String email = emailField.getText();
                String stanCywilny = String.valueOf(stancomboBoc.getSelectedItem());
                if (!Mężczyzna.isSelected() && !Kobieta.isSelected()) {
                    throw new IllegalArgumentException("wybierz coś");
                }
                try (PrintWriter writer = new PrintWriter(new FileWriter("zapis.txt"))) {
                    writer.println("Imie " + imie );
                    writer.println("Nazwisko " + nazwisko);
                    writer.println("Numer buta " + numerbuta);
                    writer.println("Płeć " + plec);
                    writer.println("Wiek " + wiek);
                    writer.println("Email " + email);
                    writer.println("Stan Cywilny " + stanCywilny);

                } catch (IOException ioe) {
                    System.out.println(ioe.getMessage());
                }

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }




            }
        });
    }

    public JPanel getPanel1() {
        return panel1;
    }

}
